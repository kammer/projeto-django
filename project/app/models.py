from django.db import models

# Create your models here.
class Products(models.Model):
    __tablename__ = 'Products'
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    qtd_store = models.CharField(max_length=5)
    creation_date = models.DateField(null=True)

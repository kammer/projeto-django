from django import forms
from django.contrib.auth.models import User

from app.models import Products


class ProductForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = (
            'name',
            'description',
            'qtd_store'
        )

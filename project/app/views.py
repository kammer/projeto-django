from django.shortcuts import render, HttpResponse
from django.views import View

from app.forms import ProductForm

# Create your views here.
class index(View):
    def get(self, request, *args):
        product_form = ProductForm()
        return render(request, "index.html")